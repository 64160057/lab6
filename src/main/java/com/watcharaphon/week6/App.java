package com.watcharaphon.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank watcharaphon = new BookBank("Watcharaphon", 50.0);  // Constructor
        watcharaphon.print();
        BookBank prayud = new BookBank("PraYuddd", 100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();
        watcharaphon.deposit(40000.0);
        watcharaphon.print();

        BookBank prawit = new BookBank("Prawit", 1000000.0);
        prawit.print();
        prawit.deposit(20);
        prawit.print();
    }
}
